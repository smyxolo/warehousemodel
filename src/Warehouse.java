import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Warehouse {
    private HashMap<ProductClass, ArrayList<Product>> productMapByClass = fillProductMapByClass();
    private HashMap<ProductType, ArrayList<Product>> productMapByType = fillProductMapByType();


    //getByType, getByClass, getByName, getPriceRange

    //namePriceType

    private HashMap<ProductType, ArrayList<Product>> fillProductMapByType(){
        HashMap<ProductType, ArrayList<Product>> tempMap = new HashMap<>();
        for (ProductType type: ProductType.values()) {
            tempMap.put(type, new ArrayList<>());
        }
        return tempMap;
    }

    private HashMap<ProductClass, ArrayList<Product>> fillProductMapByClass(){
        HashMap<ProductClass, ArrayList<Product>> tempMap = new HashMap<>();
        for (ProductClass productClass : ProductClass.values()){
            tempMap.put(productClass, new ArrayList<>());
        }
        return tempMap;

    }

    public void addProduct(String name, double price, ProductClass productClass, ProductType productType){
        productMapByClass.get(productClass).add(new Product(name, price, productType, productClass));
        productMapByType.get(productType).add(new Product(name, price, productType, productClass));
    }

    public ArrayList<Product> getByType(ProductType productType){
        return productMapByType.get(productType);
    }

    public ArrayList<Product> getByClass(ProductClass productClass){
        return productMapByClass.get(productClass);
    }

    public boolean checkType(String name, ProductType TYPE){
        boolean isOfGivenType = false;
        for (Product produkt : productMapByType.get(TYPE)) {
            if(produkt.getName().equals(name)){
                isOfGivenType = true;
            }
        }
        return isOfGivenType;
    }

    public boolean checkClass(String name, ProductClass CLASS){
        boolean isOfGivenClass = false;
        for (Product produkt : productMapByClass.get(CLASS)) {
            if(produkt.getName().equals(name)){
                isOfGivenClass = true;
            }
        }
        return isOfGivenClass;
    }

    public boolean checkProductExistence(String name){
        boolean exists = false;
        for (ProductClass Class: ProductClass.values()) {
            for (Product product : productMapByClass.get(Class)) {
                if (product.getName().equals(name)){
                    exists = true;
                }
            }
        }
        return exists;
    }

    public HashMap<ProductClass, ArrayList<Product>> getProductMapByClass() {
        return productMapByClass;
    }

    public HashMap<ProductType, ArrayList<Product>> getProductMapByType() {
        return productMapByType;
    }
}
